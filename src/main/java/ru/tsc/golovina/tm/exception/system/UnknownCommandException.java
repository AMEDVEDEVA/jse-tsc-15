package ru.tsc.golovina.tm.exception.system;

import ru.tsc.golovina.tm.constant.TerminalConst;
import ru.tsc.golovina.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use ``" + TerminalConst.HELP + "`` to display list of commands.");
    }

}
