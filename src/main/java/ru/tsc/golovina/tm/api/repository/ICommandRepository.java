package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
